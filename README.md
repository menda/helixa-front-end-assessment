# README

Here is a brief presentation of the app and some considerations on how I would proceed further with the project

## The app

On startup, users are shown a list of NBA players

![](./assets/home.png)

By using the search bar, they can search for their favorite player

![](./assets/search.png)

On tap over the player, users will be able to read more info about the selected player

![](./assets/detail.png)

## How to run

`yarn start`

## Considerations

Here are some considerations I would take if I were to undertake this project for real:

1. Dependencies are naively treated as of yet. I would consider an IoC container or other mechanism to make sure we can do dependency inversion and injection. Most importantly, async thunks are now importing directly the API client, instead of having it injected. This makes it hard to test and binds it to a specific implementation of the API client

2. In a real app, views should implement both a loading and a failure state, given their content is almost entirely dependent on API calls. I would add loading and failure flags in the reducers

3. My components implement the UI as well, and styles are repeated. I would consider building a small component library, for basic UI elements such as buttons, inputs, and so on. I would also consider grouping together theming configurations, such as colors and spacing.

4. Given more time, I would have found a way to properly test the untested components. More importantly, I would need to find a way to inject dependencies in the view, in order to properly mock the data fetching functions.
