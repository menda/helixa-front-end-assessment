import React from 'react';
import { render } from '@testing-library/react';
import { PlayerStats } from '../PlayerStats';
import { SeasonAverages } from '../../../../types';

describe('given a PlayerStats component', () => {
    const mockSeasonAverages = {
        games_played: 12,
        ast: 30,
        fg3a: 100,
    } as unknown as SeasonAverages;

    describe('when no averages are provided', () => {
        it('should render nothing', () => {
            const { queryByRole } = render(
                <PlayerStats />,
            );

            expect(queryByRole('heading')).not.toBeInTheDocument();
        });
    });

    describe('when averages are available', () => {
        it('should map the supported averages to the expected label', () => {
            const { getByText } = render(
                <PlayerStats averages={mockSeasonAverages} />,
            );

            expect(getByText('ast')).toBeInTheDocument();
            expect(getByText(30)).toBeInTheDocument();

            expect(getByText('gp')).toBeInTheDocument();
            expect(getByText(12)).toBeInTheDocument();
        });

        it('should not render the unsupported averages', () => {
            const { queryByText } = render(
                <PlayerStats averages={mockSeasonAverages} />,
            );

            expect(queryByText('fg3a')).not.toBeInTheDocument();
        });
    })
});
