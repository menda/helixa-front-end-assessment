import React, { useCallback } from "react";
import { SeasonAverages } from "../../../types";

type PlayerStat = {
    label: string;
    value: string | number;
}

type Props = {
    averages?: SeasonAverages;
}

const labelsMap: Record<string, string> = {
    games_played: 'gp',
    min: 'min',
    fgm: 'fgm',
    fga: 'fga',
    ftm: 'ftm',
    fta: 'fta',
    reb: 'reb',
    ast: 'ast',
    stl: 'stl',
    pts: 'pts',
}

function toPlayerStat([name, value]: [string, number | string]): PlayerStat | null {
    const label = labelsMap[name];

    if (!label) {
        return null;
    }

    return {
        label,
        value
    }
}

export function PlayerStats({ averages }: Props) {
    const renderAverage = useCallback((stat: { label: string, value: number | string }) => {
        return (
            <div className="PlayerDetail-stats-section" key={stat.label}>
                <h4 className="PlayerDetail-stats-section__title">{stat.label}</h4>
                <h4 className="PlayerDetail-stats-section__value">{stat.value}</h4>
            </div>
        )
    }, [])

    if (!averages) {
        return null
    };

    const stats = Object.entries(averages)
        .map(toPlayerStat)
        .filter(Boolean) as PlayerStat[]

    return (
        <div className="PlayerDetail-stats">
            <h3 className="PlayerDetail-stats__title">Season averages</h3>
            {stats.map(renderAverage)}
        </div>
    )
}