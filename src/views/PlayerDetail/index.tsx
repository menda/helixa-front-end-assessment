import React, { useCallback, useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { useAppDispatch, useAppSelector } from '../../store/hooks';
import { fetchSeasonAverages, playerByIdSelector, seasonAveragesSelector } from '../../store/reducers/players';
import { PlayerStats } from './components/PlayerStats';
import './index.css';

type PlayerRouteParams = {
  playerId?: string;
}

function formatHeight(height_feet: number, height_inches: number) {
  if (!height_feet || !height_inches) {
    return `N/A`
  }

  return `${height_feet}'${height_inches}''`
}

function formatWeight(weight: number) {
  if (!weight) {
    return `N/A`
  }

  return `${weight}lb`
}

export function PlayerDetail() {
  const dispatch = useAppDispatch()
  const { playerId } = useParams<PlayerRouteParams>();
  const player = useAppSelector(playerByIdSelector(playerId))
  const averages = useAppSelector(seasonAveragesSelector)
  const [imageAvailable, setImageAvailable] = useState(true);

  useEffect(() => {
    if (!player) {
      return
    }

    dispatch(fetchSeasonAverages(player.id))
  }, [dispatch, player])

  const onError = useCallback(() => {
    setImageAvailable(false);
  }, [])

  if (!player) {
    return null;
  }

  const playerImage = imageAvailable ?
    {
      alt: `Picture of ${player.first_name} ${player.last_name}`,
      src: `https://nba-players.herokuapp.com/players/${player.last_name}/${player.first_name}`
    } : null

  return (
    <div className="PlayerDetail">
      <section className="PlayerDetail-masthead">
        <h2 className="PlayerDetail-name">
          {player.first_name}
          {' '}
          {player.last_name}
        </h2>
        {playerImage ? <img className="PlayerDetail-image"
          alt={playerImage.alt}
          src={playerImage.src}
          onError={onError} /> : null}
        <div className="PlayerDetail-info-sections">
          <div className="PlayerDetail-info-section">
            <h4 className="PlayerDetail-info-section__title">Height</h4>
            <h4 className="PlayerDetail-info-section__value">{formatHeight(player.height_feet, player.height_inches)}</h4>
          </div>
          <div className="PlayerDetail-info-section">
            <h4 className="PlayerDetail-info-section__title">Weight</h4>
            <h4 className="PlayerDetail-info-section__value">{formatWeight(player.weight_pounds)}</h4>
          </div>
          <div className="PlayerDetail-info-section">
            <h4 className="PlayerDetail-info-section__title">Team</h4>
            <h4 className="PlayerDetail-info-section__value">{player.team.abbreviation}</h4>
          </div>
          <div className="PlayerDetail-info-section">
            <h4 className="PlayerDetail-info-section__title">Position</h4>
            <h4 className="PlayerDetail-info-section__value">{player.position}</h4>
          </div>
        </div>
        <PlayerStats averages={averages} />
      </section>
    </div>
  );
}


