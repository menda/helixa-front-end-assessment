import { TextField, Button, Card } from "@material-ui/core";
import React, { useState, useEffect, useCallback, ChangeEvent, FormEvent } from "react";
import { Link } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../store/hooks";
import { playersSelector, fetchPlayers } from "../../store/reducers/players";
import { Player } from "../../types";

import './index.css';

const linkStyles = {
    textDecoration: 'none',
    color: 'currentColor',
}

export function PlayersList() {
    const dispatch = useAppDispatch()
    const { players } = useAppSelector(playersSelector)

    const [searchText, setSearchText] = useState('')

    useEffect(() => {
        dispatch(fetchPlayers());
    }, [dispatch])

    const onChange = useCallback((evt: ChangeEvent<HTMLInputElement>) => {
        setSearchText(evt.target.value)
    }, [])

    const onSubmit = useCallback((evt: FormEvent) => {
        evt.preventDefault();
        dispatch(fetchPlayers(searchText))
    }, [dispatch, searchText])

    const renderPlayer = useCallback((player: Player) => {
        return (
            <Card className="PlayersList-item" >
                <Link
                    style={linkStyles}
                    key={player.id}
                    to={`/player/${player.id}`}
                >
                    {player.first_name}
                    {' '}
                    {player.last_name}
                    {' - '}
                    {player.team.abbreviation}
                </Link>
            </Card>
        )
    }, [])

    return (
        <>
            <form className="PlayersList-form" onSubmit={onSubmit}>
                <TextField
                    id="search"
                    className="PlayersList-input"
                    label="Search"
                    value={searchText}
                    onChange={onChange}
                />
                <Button
                    className="PlayersList-button"
                    variant="contained"
                    type="submit"
                >
                    Search
                </Button>
            </form>
            <div className="PlayersList-list">
                {players.map(renderPlayer)}
            </div>
        </>
    );
}