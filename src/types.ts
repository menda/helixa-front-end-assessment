type Team = {
  id: number;
  abbreviation: string;
  city: string;
  conference: string;
  division: string;
  full_name: string;
  name: string;
}

export type Player = {
  id: number;
  first_name: string;
  last_name: string;
  position: string;
  height_feet: number;
  height_inches: number;
  weight_pounds: number;
  team: Team;
};

export type SeasonAverages = {
  games_played: number;
  min: string;
  fgm: number;
  fga: number;
  ftm: number;
  fta: number;
  reb: number;
  ast: number;
  stl: number;
  pts: number;
}