import React from 'react';
import { Provider } from 'react-redux';
import {
  BrowserRouter as Router,
  Link,
  Route, Switch
} from 'react-router-dom';
import './App.css';
import logo from './assets/logo.svg';
import { store } from './store';
import { PlayerDetail } from './views/PlayerDetail';
import { PlayersList } from './views/PlayersList';

const linkStyles = {
  textDecoration: 'none',
  color: 'currentColor',
}

function App() {
  return (
    <Provider store={store}>
      <Router>
        <div className="App">
          <header className="App-header">
            <Link style={linkStyles} to="/">
              <img src={logo} className="App-logo" alt="logo" />
              <h1 className="App-title">
                NBA players
              </h1>
            </Link>
          </header>
          <main className="App-wrapper">
            <Switch>
              <Route exact path="/">
                <PlayersList />
              </Route>
              <Route path="/player/:playerId">
                <PlayerDetail />
              </Route>
            </Switch>
          </main>
        </div>
      </Router>
    </Provider>
  )
}

export default App;
