import {AxiosInstance} from 'axios';
import {APIClient, IAPIClient} from '../api-client';

describe('given an API client', () => {
  let httpClient: AxiosInstance;
  let subject: IAPIClient;

  beforeEach(() => {
    httpClient = ({
      get: jest.fn(),
      post: jest.fn(),
      put: jest.fn(),
      delete: jest.fn(),
    } as unknown) as AxiosInstance;
    subject = new APIClient(httpClient);
  });

  describe('when a GET request is executed', () => {
    it('should forward it to the http client', async () => {
      await subject.get('/any-route');

      expect(httpClient.get).toHaveBeenCalledWith('/any-route');
    });
  });

  describe('when a POST request is executed', () => {
    it('should forward it to the http client', async () => {
      const mockData = {
        some: 'data',
      };

      await subject.post('/any-route', mockData);

      expect(httpClient.post).toHaveBeenCalledWith('/any-route', mockData);
    });
  });

  describe('when a PUT request is executed', () => {
    it('should forward it to the http client', async () => {
      const mockData = {
        some: 'data',
      };

      await subject.put('/any-route', mockData);

      expect(httpClient.put).toHaveBeenCalledWith('/any-route', mockData);
    });
  });

  describe('when a DELETE request is executed', () => {
    it('should forward it to the http client', async () => {
      await subject.delete('/any-route');

      expect(httpClient.delete).toHaveBeenCalledWith('/any-route');
    });
  });
});
