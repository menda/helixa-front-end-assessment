/**
 * The APIClient is meant to provide an abstracted
 * way of hitting the server.
 *
 * In its simplest form, it's just a wrapper over axios.
 *
 * In a real code base, this can be decorated to
 * handle authentication, caching and app-wide error handling
 */

import {AxiosInstance} from 'axios';

type Response<T> = {
  data: T;
};

export interface IAPIClient {
  get<T>(url: string): Promise<Response<T>>;
  post<T>(url: string, body: Record<string, unknown>): Promise<Response<T>>;
  put<T>(url: string, body: Record<string, unknown>): Promise<Response<T>>;
  delete(url: string): Promise<Response<unknown>>;
}

export class APIClient implements IAPIClient {
  private http;

  constructor(httpClient: AxiosInstance) {
    this.http = httpClient;
  }

  get(url: string) {
    return this.http.get(url);
  }

  post(url: string, body: Record<string, unknown>) {
    return this.http.post(url, body);
  }

  put(url: string, body: Record<string, unknown>) {
    return this.http.put(url, body);
  }

  delete(url: string) {
    return this.http.delete(url);
  }
}
