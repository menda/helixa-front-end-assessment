/**
 * This is just a naive approach to construct
 * and export dependencies across the codebase.
 *
 * I would consider an IoC container or other mechanism
 * to perform dependency inversion and injection.
 */

import axios from 'axios';
import {APIClient} from './api-client';

const instance = axios.create({
  baseURL: 'https://www.balldontlie.io/api/v1',
});

export const apiClient = new APIClient(instance);
