import {Player, SeasonAverages} from '../../../types';
import { fetchPlayers, fetchSeasonAverages, playersReducer } from '../players';

const mockPlayers: Player[] = [
  {
    id: 1,
    first_name: 'someName',
    last_name: 'someSurname',
    height_feet: 6,
    height_inches: 7,
    position: 'F',
    weight_pounds: 120,
    team: {
      abbreviation: 'LAL',
      city: 'Los Angeles',
      conference: 'West',
      division: 'Pacific',
      full_name: 'Los Angeles Lakers',
      id: 1,
      name: 'Lakers'
    },
  }
];

const mockSeasonAverages: SeasonAverages = {
  games_played: 42,
  min: '33:51',
  fgm: 42,
  fga: 42,
  ftm: 42,
  fta: 42,
  reb: 42,
  ast: 42,
  stl: 42,
  pts: 42,
}

describe('given a players reducer', () => {
  let subject: typeof playersReducer;

  beforeEach(() => {
    subject = playersReducer;
  });

  describe('when the players list has been fetched', () => {
    it('should store the players list', () => {
      const newState = subject(
        undefined,
        fetchPlayers.fulfilled(mockPlayers, '', undefined),
      );

      const expectedState: typeof newState = {
        players: mockPlayers,
      };

      expect(newState).toEqual(expectedState);
    });
  });

  describe('when the season averages have been fetched', () => {
    it('should store the averages', () => {
      const newState = subject(
        undefined,
        fetchSeasonAverages.fulfilled(mockSeasonAverages, '', 1),
      );

      const expectedState: typeof newState = {
        players: [],
        seasonAverages: mockSeasonAverages,
      };

      expect(newState).toEqual(expectedState);
    });
  });
});
