import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { Player, SeasonAverages } from '../../types';
import { RootState } from '..';
import { apiClient } from '../../lib/services';

export const fetchPlayers = createAsyncThunk('players', async (
  filter?: string,
) => {
  const searchString = filter ? `?search=${filter}` : ''
  const { data } = await apiClient.get(`/players${searchString}`);
  return data.data || [];
}); 

export const fetchSeasonAverages = createAsyncThunk('season_averages', async (
  playerId: number,
) => {
  const { data } = await apiClient.get(`/season_averages?player_ids[]=${playerId}`);
  return data.data[0];
}); 

interface PlayersState {
  players: Player[];
  selectedPlayerId?: Player['id'];
  seasonAverages?: SeasonAverages;
}

const initialState: PlayersState = {
  players: [],
};

const { reducer } = createSlice({
  name: 'players',
  initialState,
  reducers: {},
  extraReducers: builder => {
    builder.addCase(fetchPlayers.fulfilled, (state, action) => {
      state.players = action.payload
    });

    builder.addCase(fetchSeasonAverages.fulfilled, (state, action) => {
      state.seasonAverages = action.payload;
    })
  },
});

export const playersSelector = (state: RootState) => state.players;
export const playerByIdSelector = (id?: number | string) => (state: RootState) =>
  state.players.players.find(
    player => player.id === Number(id),
  );
export const seasonAveragesSelector = (state: RootState) => state.players.seasonAverages;

export { reducer as playersReducer };
